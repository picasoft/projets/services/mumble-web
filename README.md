## Interface Mumble web

Ce dossier contient les fichiers nécessaires pour lancer un serveur web facilitant l'accès au serveur Mumble que nous hébergeons.

### Lancement

Il suffit de lancer un `docker-compose up -d`.
Il faudra veiller à ce qu'un serveur Mumble soit associé, voir configuration.

### Configuration

Voir [config.json](./config.json).

Le serveur Mumble associé est configuré via la variable d'environnement `MUMBLE_SERVER` du fichier [docker-compose.yml](./docker-compose.yml).
Il est préférable d'utiliser une URL qu'un nom de conteneur, pour permettre à l'interface web de tourner sur une machine différente du serveur Mumble.

### Mise à jour

La mise à jour se fait via le [Dockerfile](./Dockerfile). Puisqu'elle ne se base sur aucune version précise pour le moment, et même sur une branche modifiée pour utiliser un thème Framasoft, on veillera à créer un fichier `CHANGELOG.md` et à choisir un tag "maison" pour décrire les changements effectués.
